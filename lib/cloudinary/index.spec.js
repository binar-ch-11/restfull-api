const { uploadImage } = require('./index');

jest.mock('cloudinary', () => ({
  v2: {
    config: jest.fn(),
    uploader: {
      upload: jest.fn().mockResolvedValue({
        asset_id: '408d492d09fb3cbe83a23d51843f612c',
        public_id: 'Screenshot_2022-12-09_at_7.58.58_PM_a4hra6',
        format: 'png',
      }),
    },
  },
}));

describe('cloudinary', () => {
  describe('uploadImage', () => {
    it('should return response', async () => {
      const upload = await uploadImage('imagePath');

      expect(upload).toMatchObject({
        asset_id: '408d492d09fb3cbe83a23d51843f612c',
        public_id: 'Screenshot_2022-12-09_at_7.58.58_PM_a4hra6',
        format: 'png',
      });
    });
  });
});
