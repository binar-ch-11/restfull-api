const cloudinary = require('cloudinary').v2;

const uploadImage = async (imagePath) => {
  const options = {
    use_filename: true,
    unique_filename: true,
    overwrite: false,
    resource_type: 'raw',
  };

  try {
    // Upload the image
    const result = await cloudinary.uploader.upload(imagePath, options);
    return result;
  } catch (error) {
    return error;
  }
};

const getAssetInfo = async (publicId) => {
  const options = {
    colors: true,
  };

  try {
    const result = await cloudinary.api.resource(publicId, options);
    console.log(result);
    return result;
  } catch (error) {
    return error;
  }
};

module.exports = { uploadImage, getAssetInfo };
