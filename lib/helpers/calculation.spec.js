const { calculation } = require('./calculation');

it('Function that adds two numbers and return sum', () => {
  expect(calculation(4, 5)).toBe(9);
  expect(calculation(4, 5)).not.toBe(10);
});
