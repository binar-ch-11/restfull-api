const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');

const { Users } = require('../../models');

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader('authorization'),
      secretOrKey: 'secret',
    },
    async (payload, done) => {
      // pengambilan data detail user
      const user = await Users.findByPk(payload.id);

      if (user) done(null, user);
      else done(null, false);
    },
  ),
);

module.exports = passport;
