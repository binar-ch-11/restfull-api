const fs = require('fs');
const { Users } = require('../../models');
const { uploadImage } = require('../../lib/cloudinary');

exports.user = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });

  res.json({
    data: user,
  });
};

exports.Updateuser = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });
  const upload = await uploadImage(req.file.path);
  await user.update(upload);
  await user.save();

  try {
    fs.unlinkSync(`public/img/${req.file.filename}`);
  } catch (err) {
    console.error(err);
  }

  res.json({
    data: user,
  });
};

exports.users = async (req, res) => {
  const users = await Users.findAll();
  res.json({
    data: users,
  });
};

exports.register = async (req, res) => {
  const { name, email, password } = req.body;
  const user = await Users.findOne({
    where: { email },
  });

  const validateNameUser = await Users.findOne({
    where: { name },
  });

  if (user) {
    res.status(401).json({
      result: 'failed',
      message: 'email sudah ada',
    });
    return;
  }
  if (validateNameUser) {
    res.status(402).json({
      result: 'failed',
      message: 'username sudah ada',
    });
    return;
  }

  const newUser = await Users.create({
    name,
    email,
    password,
    is_online: false,
    image: 'https://res.cloudinary.com/dyx8tahem/image/upload/v1671104741/gambar-Wa-1_1_gpkpgn.png',
    updatedAt: new Date(),
  });

  res.json({
    data: newUser,
  });
};

exports.updateProfile = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });
  await user.update({ image: req.body.secure_url });
  await user.save();
  res.json({
    data: user,
  });
};
