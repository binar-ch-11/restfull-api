const fs = require('fs');
const { Messages } = require('../../models');
const { uploadImage } = require('../../lib/cloudinary');

exports.messages = async (req, res) => {
  const messages = await Messages.findAll();
  const sortMessage = messages.sort((objA, objB) => Number(objA.dataValues.createdAt) - Number(objB.dataValues.createdAt));
  res.json({
    data: sortMessage,
  });
};

exports.sendMessage = async (req, res) => {
  let pathImage = '';
  let isVideoFile = false;
  if (req.file) {
    const upload = await uploadImage(req.file.path);

    const mediaHandlingDetect = (param) => {
      const result = param.split('/');
      return result[0];
    };

    const typeMedia = mediaHandlingDetect(req.file.mimetype);
    if (typeMedia === 'video') {
      isVideoFile = true;
    }

    pathImage = upload.secure_url;
    try {
      fs.unlinkSync(`public/img/${req.file.filename}`);
    } catch (err) {
      console.error(err);
    }
  }

  await Messages.create({
    user_id: req.body.user_id,
    message: req.body.message,
    media: req.file ? pathImage : null,
    is_file: isVideoFile,
  });
  res.json({
    status: 'SUCCESS',
  });
};
