const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const { Users } = require('../../models');

exports.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await Users.findOne({
    where: { email, password },
  });

  if (!user) {
    res.status(402).json({
      result: 'Failed',
      message: 'username not found!',
    });
  } else {
    await user.update({ is_online: true });
    await user.save();
    const { name, id } = user;
    const token = jwt.sign(
      {
        id,
        name,
      },
      'secret',
    );

    const response = {
      name,
      id,
      accessToken: token,
    };
    res.send(response);
  }
};

exports.logout = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });

  const jakartaDate = moment().tz('Asia/Jakarta').format();
  await user.update({ is_online: false, updatedAt: new Date(jakartaDate) });
  await user.save();

  res.json({
    status: 'Logout Success',
  });
};
