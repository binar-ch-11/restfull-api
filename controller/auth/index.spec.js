const { logout, login } = require('./index');
const { Users } = require('../../models');

jest.mock('jsonwebtoken');
jest.mock('../../models');
jest.mock('../users');

describe('Auth', () => {
  it('Logout /logout', async () => {
    const res = { json: jest.fn() };
    Users.findOne.mockResolvedValueOnce({ update: jest.fn(), save: jest.fn() });

    await logout({ user: { id: 1 } }, res);

    expect(res.json).toBeCalled();
  });

  it('Login /login', async () => {
    const res = { send: jest.fn() };
    Users.findOne.mockResolvedValueOnce({ update: jest.fn(), save: jest.fn() });

    await login({ body: { email: 'email', password: 'password' } }, res);
    expect(res.send).toBeCalled();
  });

  it('Login error handling /login', async () => {
    const res = {
      json: jest.fn(),
      status: 402,
      send: jest.fn(),
    };
    Users.findOne.mockResolvedValueOnce({
      update: jest.fn(),
      save: jest.fn(),
    });

    await login({ body: { email: null, password: null } }, res);
    expect(res.status);
  });
});
