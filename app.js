const express = require('express');
const multer = require('multer');
const cors = require('cors');
const bodyParser = require('body-parser');
const { createServer } = require('http');
const { Server } = require('socket.io');
const cloudinary = require('cloudinary').v2;

require('dotenv').config();

const { Users, Messages } = require('./models');
const passport = require('./lib/passport');
const authController = require('./controller/auth');
const usersController = require('./controller/users');
const messagesController = require('./controller/messages');

const app = express();
const port = process.env.PORT || 3001;
app.use(cors());
app.use(bodyParser.json());
app.use('/public', express.static('public'));
app.use(passport.initialize());

const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: process.env.SOCKET_IO_CONNECT_NETLIFY,
    methods: ['GET', 'POST'],
  },
});

cloudinary.config({
  cloud_name: 'dyx8tahem',
  api_key: '981854646411735',
  api_secret: 'YLs61JPtsuSjyIwnvMEdCjv6Hl8',
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/img/');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

io.on('connection', (socket) => {
  socket.on('send_message', async () => {
    const allMessage = await Messages.findAll();
    const sortMessage = allMessage.sort((objA, objB) => Number(objA.dataValues.createdAt) - Number(objB.dataValues.createdAt));
    io.emit('receive_message', sortMessage);
  });

  socket.on('login_logout', async () => {
    const allUsers = await Users.findAll();
    io.emit('status_user', allUsers);
  });
});

app.post(
  '/send-message',
  upload.single('image_message'),
  messagesController.sendMessage,
);

app.get('/all-message', messagesController.messages);

app.get('/all-user', usersController.users);

app.post('/register', upload.single('profile_image'), usersController.register);

app.post('/login', authController.login);

app.get(
  '/logout',
  passport.authenticate('jwt', { session: false }),
  authController.logout,
);

app.get(
  '/user',
  passport.authenticate('jwt', { session: false }),
  usersController.user,
);

app.post(
  '/update-profile',
  passport.authenticate('jwt', { session: false }),
  usersController.updateProfile,
);

httpServer.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

module.exports = { app };
