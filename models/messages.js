const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Messages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Messages.belongsTo(models.Users, {
        foreignKey: 'user_id',
      });
    }
  }
  Messages.init(
    {
      user_id: DataTypes.INTEGER,
      message: DataTypes.STRING,
      media: DataTypes.STRING,
      is_file: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'Messages',
    },
  );
  return Messages;
};
