const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Users.hasOne(models.Messages, {
        foreignKey: 'user_id',
      });
    }
  }
  Users.init(
    {
      name: DataTypes.STRING,
      image: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      is_online: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'Users',
    },
  );
  return Users;
};
